<?php

include_once("../src/db.php");

$product_id = $_GET['product_id'];

if(isset($product_id)){
    $query = "UPDATE products SET is_active = 1 WHERE product_id = :product_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $result = $sth->execute();

    if($result){

        session_start();

        $_SESSION['activated'] = "<div class='alert alert-success'>Product activated successfully.</div>";
        header("location:inactive.php");
    }else{
        $_SESSION['activated'] = "<div class='alert alert-danger'>Product is not activated!</div>";
    }
}



