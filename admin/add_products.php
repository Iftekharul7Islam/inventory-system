<?php
include_once("../src/db.php");

if($_SERVER['REQUEST_METHOD'] = 'POST' && isset($_POST['add-product'])) {

    /*echo '<pre>';
    print_r($_POST);
    echo '</pre>';
    die();*/

    $product_name = $_POST['product_name'];
    $category_id = $_POST['category_id'];
    $brand_id = $_POST['brand_id'];
    $mrp = $_POST['mrp'];

    $is_active = 0;
    if(array_key_exists('is_active', $_POST)){
        $is_active = $_POST['is_active'];
    }



    if(empty($product_name) && empty($category_id) && empty($brand_id) && empty($mrp)){
        $errors[] = "<div class='alert alert-danger'>Fields must not be empty!</div>";
    }elseif(empty($product_name) || empty($category_id) || empty($brand_id) || empty($mrp)){
        $errors[] = "<div class='alert alert-danger'>Field must not be empty!</div>";
    }else{

        $time_zone = date_default_timezone_set("Asia/Dhaka");

        $query = "INSERT INTO products (product_name, category_id, brand_id, mrp, is_active, created_at, modified_at) 
VALUES (:product_name, :category_id, :brand_id, :mrp, :is_active, :created_at, :modified_at)";

        $sth = $conn->prepare($query);
        $sth->bindParam(':product_name', $product_name);
        $sth->bindParam(':category_id', $category_id);
        $sth->bindParam(':brand_id', $brand_id);
        $sth->bindParam(':mrp', $mrp);
        $sth->bindParam(':is_active', $is_active);
        $sth->bindParam(':created_at', date('Y-m-d h:i:s'));
        $sth->bindParam(':modified_at', date('Y-m-d h:i:s'));
        $result = $sth->execute();

        if($result){
            session_start();

            $_SESSION['inserted'] = "<div class='alert alert-success'>Product inserted successfully.</div>";

            header("location:manage_products.php");
        }else{
            $_SESSION['inserted'] = "<div class='alert alert-danger'>Product not inserted!</div>";
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Products</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 30rem;">
        <div class="card-header"><h5>Add New Product</h5></div>
        <div class="card-body">

            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            ?>

<form action="" method="post">
    <div class="form-group">
        <input
            type="text"
            name="product_name"
            class="form-control"
            id="product_name"
            placeholder="Enter Product Name"
            autofocus="autofocus">
        </div>

    <div class="form-group">
        <select
            name="category_id"
            id="category_id"
            class="form-control">
            <option>Select Category...</option>
            <?php
            $query = "SELECT * FROM categories ORDER BY category_id DESC";
            $sth = $conn->prepare($query);
            $sth->execute();
            $categories = $sth->fetchAll(PDO::FETCH_ASSOC);

            if($categories){
            foreach($categories as $category){
            ?>
            <option value="<?= $category['category_id'];?>"><?= $category['category_name'];?></option>
            <?php }}?>
        </select>

            </div>

    <div class="form-group">
        <select
            name="brand_id"
            id="brand_id"
            class="form-control">
            <option>Select Brand...</option>
            <?php
            $query = "SELECT * FROM brands ORDER BY brand_id DESC";
            $sth = $conn->prepare($query);
            $sth->execute();
            $brands = $sth->fetchAll(PDO::FETCH_ASSOC);

            if($brands){
                foreach($brands as $brand){
                    ?>
                    <option value="<?= $brand['brand_id'];?>"><?= $brand['brand_name'];?></option>
                <?php }}?>
        </select>
    </div>

    <div class="form-group">
        <input
            type="text"
            name="mrp"
            class="form-control"
            id="mrp"
            placeholder="Enter Price(MRP)">
    </div>

    <div class="form-group">
        <label>Active</label>
        <input type="checkbox"
               checked="checked"
               value="1"
               name="is_active"
               class="form-control"
               id="is_active">
    </div>

    <button type="submit" class="btn btn-dark" name="add-product">Save</button>
</form>


        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>