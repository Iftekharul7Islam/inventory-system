<?php
session_start();

include_once("../src/db.php");

if($_SESSION['id'] == null){
    header('Location: login.php');
}

/*echo "<pre>";
print_r($result);
echo "</pre>";
die();*/

$product_id = $_POST['product_id'];

if(!empty($product_id)){

    $query = 'SELECT * FROM products WHERE product_id = :product_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);


    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add-to-order'])) {

        $s_id =  $_SESSION['guest_user'];
        $product_id = $product['product_id'];
        $product_name = $product['product_name'];
        $unit_price = $product['mrp'];
        $quantity = $_POST['quantity'];
        $total_price = $quantity * $unit_price;


        if(!empty($s_id) && !empty($product_id)){

            $query = 'SELECT * FROM invoices WHERE s_id = :s_id AND product_id = :product_id';
            $sth = $conn->prepare($query);
            $sth->bindParam(':s_id', $s_id);
            $sth->bindParam(':product_id', $product_id);
            $sth->execute();

            $order_row = $sth->fetch(PDO::FETCH_ASSOC);

            if($order_row > 0){

                //die('the product is in the order list!');

                $final_quantity = $quantity + $order_row['quantity'];
                $total_price = $final_quantity * $order_row['unit_price'];

                $query = "UPDATE invoices
                          SET 
                          quantity = :quantity, 
                          total_price = :total_price
          
                          WHERE product_id = :product_id AND s_id = :s_id";

                $sth = $conn->prepare($query);
                $sth->bindParam(':quantity', $final_quantity);
                $sth->bindParam(':total_price', $total_price);
                $sth->bindParam(':product_id', $product_id);
                $sth->bindParam(':s_id', $s_id);
                $result = $sth->execute();
                
                if($result){

                    header('Location:orderlist.php');
                }

            }else{

                if (empty($s_id) && empty($product_id) && empty($product_name) && empty($unit_price) && empty($quantity) && empty($total_price)) {
                    $errors[] = "<div class='alert alert-danger'>Fields must not be empty!</div>";
                } elseif (empty($s_id) || empty($product_id) || empty($product_name) || empty($unit_price) || empty($quantity) || empty($total_price)) {
                    $errors[] = "<div class='alert alert-danger'>Field must not be empty!</div>";
                } else {
                    $query = "INSERT INTO invoices (s_id, product_id, product_name, unit_price, quantity, total_price) 
                              VALUES (:s_id, :product_id, :product_name, :unit_price, :quantity, :total_price)";

                    $sth = $conn->prepare($query);
                    $sth->bindParam(':s_id', $s_id);
                    $sth->bindParam(':product_id', $product_id);
                    $sth->bindParam(':product_name', $product_name);
                    $sth->bindParam(':unit_price', $unit_price);
                    $sth->bindParam(':quantity', $quantity);
                    $sth->bindParam(':total_price', $total_price);
                    $result = $sth->execute();

                    if($result){

                        header('Location:orderlist.php');
                    }

                }
            }

        }
    }

}


?>




