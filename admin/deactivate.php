<?php

include_once("../src/db.php");

$product_id = $_GET['product_id'];

if(isset($product_id)){
    $query = "UPDATE products SET is_active = 0 WHERE product_id = :product_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $result = $sth->execute();

    if($result){

        session_start();

        $_SESSION['deactivated'] = "<div class='alert alert-success'>Product deactivated successfully.</div>";
        header("location:active.php");
    }else{
        $_SESSION['deactivated'] = "<div class='alert alert-danger'>Product is not deactivated!</div>";
    }
}



