<?php

include_once("../src/db.php");

$brand_id = $_GET['brand_id'];

if(isset($brand_id)){
    $query = "DELETE FROM brands WHERE brand_id = :brand_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':brand_id', $brand_id);
    $result = $sth->execute();
    
    if($result){

        session_start();
        
        $_SESSION['deleted'] = "<div class='alert alert-success'>Brand name deleted successfully.</div>";
        header("location:manage_brands.php");
    }else{
        $_SESSION['deleted'] = "<div class='alert alert-danger'>Brand name is not deleted!</div>";
    }
}



