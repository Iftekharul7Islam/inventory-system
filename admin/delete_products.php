<?php

include_once("../src/db.php");

$product_id = $_GET['product_id'];

if(isset($product_id)){
    $query = "DELETE FROM products WHERE product_id = :product_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $result = $sth->execute();

    if($result){

        session_start();

        $_SESSION['deleted'] = "<div class='alert alert-success'>Product deleted successfully.</div>";
        header("location:manage_products.php");
    }else{
        $_SESSION['deleted'] = "<div class='alert alert-danger'>Product is not deleted!</div>";
    }
}



