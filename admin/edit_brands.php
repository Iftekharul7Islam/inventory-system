<?php
include_once("../src/db.php");

$brand_id = $_GET['brand_id'];
if(isset($brand_id)){

    $query = 'SELECT * FROM brands WHERE brand_id = :brand_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':brand_id', $brand_id);
    $sth->execute();

    $brand = $sth->fetch(PDO::FETCH_ASSOC);
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $brand_name = $_POST['brand_name'];

    if(empty($brand_name)){
        $errors[] = "<div class='alert alert-danger'>Brand name must not be empty!</div>";
    }else{
        $query = "UPDATE products SET brand_name = :brand_name WHERE brand_id = :brand_id";

        $sth = $conn->prepare($query);
        $sth->bindParam(':brand_id', $brand_id);
        $sth->bindParam(':brand_name', $brand_name);
        $result = $sth->execute();
        
        if($result){
            session_start();
            
            $_SESSION['updated'] = "<div class='alert alert-success'>Brand name updated successfully.</div>";
            header("location:manage_brands.php"); 
        }else{
            $_SESSION['updated'] = "<div class='alert alert-danger'>Brand name not updated!</div>";
        }

        
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Brands</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 18rem;">
        <div class="card-header">Edit Brand</div>
        <div class="card-body">
            
            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            ?>

<form action="" method="post">

    <div class="form-group">
        <input type="text"
               value="<?= $brand['brand_name']; ?>"
               name="brand_name"
               class="form-control"
               id="brand_name"
               autofocus="autofocus"
               placeholder="Enter Brand">
    </div>

    <button type="submit" class="btn btn-dark" name="add-brand"><i class="far fa-edit"></i></i> Update</button>
</form>
</div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>