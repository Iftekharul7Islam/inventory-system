<?php
session_start();

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

include_once("../src/db.php");

if(isset($_SESSION['id'])){
    header('Location: index.php');
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send-mail'])) {

    $email = $_POST['email'];

    if(empty($email)){
        $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
    }else{

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
        }else{
            $query = 'SELECT email FROM users WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);

            if (empty($row['email'])) {
                $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not exist!</div>";
            }
        }


        if(empty($errors)) {

            $query = 'SELECT * FROM users WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if($result){

                /*echo '<pre>';
                print_r($result);
                echo '</pre>';
                die();*/

                $token = $result['token'];
                $name = $result['name'];
                $email = $result['email'];

                //***************************

                // Instantiation and passing `true` enables exceptions
                $mail = new PHPMailer(true);

                try {
                    //Server settings
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'smtp.mailtrap.io';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'ccf4b98500b6aa';                     // SMTP username
                    $mail->Password   = '4c132260973429';                               // SMTP password
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                    $mail->Port       = 587;                                    // TCP port to connect to

                    //Recipients
                    $mail->setFrom('admin@eshikhon.com', 'Inventory');
                    $mail->addAddress($email, $name);     // Add a recipient
                    $mail->addReplyTo('no-reply@gmail.com', 'No Reply');


                    // Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Reset your password';
                    $mail->Body    = '<p>Hello '.$name.',</br></br>

                                        Please click on the link below to reset your password.
                                      </p><a href="http://localhost/inventory-system/admin/reset_password.php?password-token='.$token.'">Reset your Password</a>';
                    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    $mail->send();
                } catch (Exception $e) {
                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }

                //***************************



                /*
                $user_id = $result['id'];
                $user_name = $result['username'];
                $email = $result['email'];

                $text = substr('$email', 0, 6);
                $random_number = rand(10000, 99999);

                $new_password = "$text$random_number";
                $hashedPassword = password_hash($new_password, PASSWORD_BCRYPT);

                $query = "UPDATE users SET password = :password WHERE id = :id";

                $sth = $conn->prepare($query);
                $sth->bindParam(':password', $hashedPassword);
                $sth->bindParam(':id', $user_id);
                $result = $sth->execute();

                $to = "$email";
                $from = "fahimcseiiuc@gmail.com";
                $headers = "From: $from\n";
                $headers .= "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
                $subject = "Your Password";
                $message = "Your username is ".$user_name." and Password is: ".$new_password." Please visit the website for Login.";
                $send_mail =  mail($to, $subject, $message, $headers);
                */

                if($mail){
                    $msgs[] = "<div class='alert alert-success'>Please check your email to reset your password.</div>";

                }else{
                    $msgs[] = "<div class='alert alert-danger'>Email not sent!</div>";

                }


            }else{
                $msgs[] = "<div class='alert alert-danger'><strong>Sorry! </strong>Data not found!</div>";
            }

        }

    }

}



?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Login</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 18rem;">
        <img class="card-img-top mx-auto" style="width: 60%" src="../lib/img/recovery_password.png" alt="Recovery Password Icon">
        <div class="card-body">

            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            if(!empty($msgs)){
                foreach($msgs as $msg){
                    echo $msg;
                }
            }

            ?>
            <p>Please enter the email address you used to sign-up</p>

            <form action="" method="post">

                <div class="form-group">
                    <input type="email"
                           value="<?php if(!empty($errors)){echo $_POST['email'];}?>"
                           name="email"
                           class="form-control"
                           id="email"
                           autofocus="autofocus"
                           placeholder="Enter Email">
                </div>

                <button type="submit" class="btn btn-primary" name="send-mail"><i class="fas fa-sign-in-alt"></i> Send Mail</button>

            </form>
        </div>
        <div class="card-footer">Already an user? Please <a href="login.php">Login</a></div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>