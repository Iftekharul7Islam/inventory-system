<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-10 mx-auto">
            <div class="card" style="box-shadow:0 0 25px 0 lightgrey;">
                <div class="card-header">
                    <h4>New Orders</h4>
                </div>
                <div class="card-body">

                    <!--<form>
				  		<div class="form-group row">
				  			<label class="col-sm-3 col-form-label">Order Date</label>
				  			<div class="col-sm-6">
				  				<input type="text" id="order_date" name="order_date" readonly class="form-control form-control-sm" value="<?php /*echo date("Y-d-m"); */?>">
				  			</div>
				  		</div>

				  		<div class="form-group row">
				  			<label class="col-sm-3 col-form-label">Customer Name*</label>
				  			<div class="col-sm-6">
				  				<input type="text" id="cust_name" name="cust_name"class="form-control form-control-sm" placeholder="Enter Customer Name" required/>
				  			</div>
				  		</div>-->


                    <div class="card" style="box-shadow:0 0 15px 0 lightgrey;">
                        <div class="card-body">
                            <h3>Make an Order List</h3>
                            <table align="center" style="width:800px;">
                                <thead>
                                <tr class="text-center">
                                    <th>Sl No.</th>
                                    <th>Product</th>
                                    <th>Price(MRP)</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody>


                                        <tr class="text-center">
                                            <td></td>

                                            <td><input type="text" name="product_name" class="form-control form-control-sm" value="" readonly></td>

                                            <td><input type="text" name="unit_price" class="form-control form-control-sm" value="" readonly></td>

                                            <td>
                                                <form action="update_orderlist.php" method="post">
                                                    <div class="input-group">
                                                        <!--<input type="hidden" name="product_id" class="form-control form-control-sm" value="">
                                                        <input type="hidden" name="unit_price" class="form-control form-control-sm" value="">-->
                                                        <input type="number" name="quantity" class="form-control form-control-sm" value="" min="1" max="100">
                                                        <!--<button type="submit" class="btn btn-primary btn-sm">Update</button>
                                                        <div>
                                                            <a href="remove_from_orderlist.php?product_id=" class="btn btn-danger btn-sm">Remove</a>
                                                        </div>-->
                                                    </div>
                                                </form>
                                            </td>

                                            <td></td>
                                        </tr>


                                    <tr>
                                        <td colspan="5" class="jumbotron">There is no orders available now! <a href="manage_products.php" class="btn btn-success btn-sm">Click Here</a> to add one on the list. </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div> <!--Crad Body Ends-->
                    </div>

                    <div style="margin-top: 50px">

                        <div class="form-group row">
                            <label for="sub_total" class="col-sm-3 col-form-label">Sub Total</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="sub_total" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gst" class="col-sm-3 col-form-label">VAT (10%)</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="vat" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount" class="col-sm-3 col-form-label">Discount</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="discount" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="net_total" class="col-sm-3 col-form-label">Net Total</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="net_total" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="paid" class="col-sm-3 col-form-label">Paid</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="paid" class="form-control form-control-sm" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="due" class="col-sm-3 col-form-label">Due</label>
                            <div class="col-sm-6">
                                <input type="text" readonly name="due" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payment_type" class="col-sm-3 col-form-label">Payment Method</label>
                            <div class="col-sm-6">
                                <select name="payment_type" class="form-control form-control-sm">
                                    <option>Cash</option>
                                    <option>Card</option>
                                    <option>Draft</option>
                                    <option>Cheque</option>
                                </select>
                            </div>
                        </div>

                        <div align="center">
                            <input type="submit" id="order_form" style="width:150px;" class="btn btn-info" value="Order">
                            <input type="submit" id="print_invoice" style="width:150px;" class="btn btn-success d-none" value="Print Invoice">
                        </div>

                    </div>


                    <!--</form>-->
                </div>
            </div>
        </div>
    </div>
</div>
