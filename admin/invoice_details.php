<?php
session_start();

include_once("../src/db.php");

/*echo '<pre>';
print_r($_POST);
echo '</pre>';
die();*/

if($_SERVER['REQUEST_METHOD'] = 'POST' && isset($_POST['purchase'])){

    $s_id = $_SESSION['guest_user'];
    //$order_date = $_POST['order_date'];
    $customer_name = $_POST['customer_name'];
    $product_id = $_POST['product_id'];
    $product_name = $_POST['product_name'];
    $unit_price = $_POST['unit_price'];
    $quantity = $_POST['quantity'];
    $sub_total = $_POST['sub_total'];
    $vat = $_POST['vat'];
    $discount = $_POST['discount'];
    $net_total = $vat - $discount;
    $paid = $_POST['paid'];
    $due = $net_total - $paid;
    $payment_type = $_POST['payment_type'];
 /*   echo '<pre>';
print_r($_POST);
echo '</pre>';
die();*/

    if(empty($customer_name) && empty($discount) && empty($paid)){
        $_SESSION['fields-empty']='<div class="alert alert-danger"><strong>Error! </strong>Fields must not be empty!</div>';
        header('Location:orderlist.php');
    }elseif(empty($customer_name) || empty($discount) || empty($paid)){
        $_SESSION['field-empty']='<div class="alert alert-danger"><strong>Error! </strong>Field must not be empty!</div>';
        header('Location:orderlist.php');
    }else{
        $time_zone = date_default_timezone_set("Asia/Dhaka");

        $query = "INSERT INTO invoice_details 
(s_id, product_id, product_name, customer_name, unit_price, quantity, sub_total, vat, discount, net_total, paid, due, payment_type, order_date) 
VALUES (:s_id, :product_id, :product_name, :customer_name, :unit_price, :quantity, :sub_total, :vat, :discount, :net_total, :paid, :due, :payment_type, :order_date)";

        $sth = $conn->prepare($query);
        $sth->bindParam(':s_id', $s_id);
        $sth->bindParam(':product_id', $product_id);
        $sth->bindParam(':product_name', $product_name);
        $sth->bindParam(':customer_name', $customer_name);
        $sth->bindParam(':unit_price', $unit_price);
        $sth->bindParam(':quantity', $quantity);
        $sth->bindParam(':sub_total', $sub_total);
        $sth->bindParam(':vat', $vat);
        $sth->bindParam(':discount', $discount);
        $sth->bindParam(':net_total', $net_total);
        $sth->bindParam(':paid', $paid);
        $sth->bindParam(':due', $due);
        $sth->bindParam(':payment_type', $payment_type);
        $sth->bindParam(':order_date', date('Y-m-d h:i:S', time()));

        $result = $sth->execute();

        if($result){
            $_SESSION['inserted']='<div class="alert alert-success"><strong>Success! </strong>Data inserted successfully.</div>';
            header('Location:orderlist.php');
        }
    }

}


