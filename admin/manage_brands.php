<?php
session_start();
include_once("../src/db.php");

$query = "SELECT * FROM brands ORDER BY brand_id DESC";
$sth = $conn->prepare($query);
$sth->execute();

$brands = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Brand List</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 100%">
        <div class="card-header">Brand List
            <div style="float: right">
                <a href="add_brands.php" class="btn btn-dark"><i class="far fa-plus-square"></i> Add</a>
            </div>
        </div>
        <div class="card-body">

            <?php
            if(isset($_SESSION['inserted'])){
                echo $_SESSION['inserted'];
            }
            $_SESSION['inserted'] = NULL;

            ?>
            <?php
            if(isset($_SESSION['updated'])){
                echo $_SESSION['updated'];
            }
            $_SESSION['updated'] = NULL;

            ?>

            <?php
            if(isset($_SESSION['deleted'])){
                echo $_SESSION['deleted'];
            }
            $_SESSION['deleted'] = NULL;

            ?>
            <table class="table table-hover table-bordered">

                <thead>
                <tr>
                    <th scope="col">Sl No.</th>
                    <th scope="col">Brand Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                if($brands){
                    $i = 0;
                    foreach($brands as $brand){
                        $i = $i + 1;
                        ?>

                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $brand['brand_name'];?></td>
                            <td><a href="edit_brands.php?brand_id=<?= $brand['brand_id']; ?>" class="btn btn-outline-info btn-sm">Edit</a>
                                <a onclick="return confirm('Are you sure you want to delete?')" href="delete_brands.php?brand_id=<?= $brand['brand_id']; ?>" class="btn btn-outline-danger btn-sm">Delete</a></td>
                        </tr>

                        <?php
                    }}else{
                    ?>
                    <tr >
                        <td colspan="3">No Brand is available!<a href="add_brands.php">Click Here</a>to add a brand </td>
                    </tr>

                <?php }?>

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>