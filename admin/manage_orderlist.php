<?php

session_start();

include_once("../src/db.php");


/*echo "<pre>";
print_r($_POST);
echo "</pre>";
die();*/

$s_id = $_SESSION['guest_user'];


if(isset($_POST['update'])){

    $product_id = $_POST['product_id'];
    $unit_price = $_POST['unit_price'];
    $quantity = $_POST['quantity'];

    $total_price = $quantity * $unit_price;

    /*echo "<pre>";
    print_r($_POST);
    echo "</pre>";
    die();*/

    $query = "UPDATE invoices
          SET 
          quantity = :quantity, 
          total_price = :total_price
          
          WHERE product_id = :product_id AND s_id = :s_id";

    $sth = $conn->prepare($query);
    $sth->bindParam(':quantity', $quantity);
    $sth->bindParam(':total_price', $total_price);
    $sth->bindParam(':product_id', $product_id);
    $sth->bindParam(':s_id', $s_id);
    $result = $sth->execute();

    if($result){
        header('Location:orderlist.php');
    }
}


$product_id = $_GET['product_id'];

if(isset($product_id)){

    $query = "DELETE FROM invoices WHERE product_id = :product_id AND s_id = :s_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->bindParam(':s_id', $s_id);
    $result = $sth->execute();

    if($result){
        header('Location:orderlist.php');
    }
}