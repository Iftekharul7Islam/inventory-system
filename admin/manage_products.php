<?php
session_start();
include_once("../src/db.php");

$query = "SELECT products.*, categories.category_name, brands.brand_name FROM products
INNER JOIN categories ON products.category_id = categories.category_id
INNER JOIN brands ON products.brand_id = brands.brand_id
ORDER BY products.product_id DESC";

//using aliases
/*$query = "SELECT p.*, c.category_name, b.brand_name FROM products as p, categories as c, brands as b
WHERE p.category_id = c.category_id AND p.brand_id = b.brand_id
ORDER BY p.product_id DESC ";*/


$sth = $conn->prepare($query);
$sth->execute();

$products = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Product List</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 100%">
        <div class="card-header">Product List
            <div style="float: right">
                <a href="add_products.php" class="btn btn-dark btn-sm"><i class="far fa-plus-square"></i> Add</a>
                <a href="active.php" class="btn btn-dark btn-sm"><i class="far fa-plus-square"></i> Active Products</a>
                <a href="inactive.php" class="btn btn-dark btn-sm"><i class="far fa-plus-square"></i> Inactive Products</a>
            </div>
        </div>
        <div class="card-body">

            <?php
            if(isset($_SESSION['inserted'])){
                echo $_SESSION['inserted'];
            }
            $_SESSION['inserted'] = NULL;

            ?>
            <?php
            if(isset($_SESSION['updated'])){
                echo $_SESSION['updated'];
            }
            $_SESSION['updated'] = NULL;

            ?>

            <?php
            if(isset($_SESSION['deleted'])){
                echo $_SESSION['deleted'];
            }
            $_SESSION['deleted'] = NULL;

            ?>
            <table class="table table-hover table-bordered">

                <thead>
                <tr>
                    <th scope="col">Sl No.</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Price(MRP)</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                if($products){
                    $i = 0;
                    foreach($products as $product){
                        $i = $i + 1;
                        ?>

                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $product['product_name'];?></td>
                            <td><?= $product['category_name'];?></td>
                            <td><?= $product['brand_name'];?></td>
                            <td>$<?= $product['mrp'];?></td>
                            <td>
                                <?php if($product['is_active']){ ?>
                                    <button class="btn btn-success btn-sm">Active</button>
                                <?php }else{ ?>
                                    <button class="btn btn-warning btn-sm">Inactive</button>
                                <?php } ?>
                            </td>
                            <td><a href="product_details.php?product_id=<?= $product['product_id']; ?>" class="btn btn-light btn-sm">View</a>
                                <a href="edit_products.php?product_id=<?= $product['product_id']; ?>" class="btn btn-info btn-sm">Edit</a>
                                <a onclick="return confirm('Are you sure you want to delete?')" href="delete_products.php?product_id=<?= $product['product_id']; ?>" class="btn btn-danger btn-sm">Delete</a></td>
                        </tr>

                        <?php
                    }}else{
                    ?>
                    <tr>
                        <td colspan="3">No Product is available!<a href="add_products.php">Click Here</a>to add a product </td>
                    </tr>

                <?php }?>

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>