<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Product List</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>


<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-10 mx-auto">

            <div class="card" style="box-shadow:0 0 15px 0 lightgrey;">
                <div class="card-header">
                    <h4>New Orders</h4>
                </div>
                <div class="card-body">

                    <?php
                    if(!empty($errors)){
                        foreach($errors as $error){
                            echo $error;
                        }
                    }

                    if(!empty($msgs)){
                        foreach($msgs as $msg){
                            echo $msg;
                        }
                    }

                    ?>

                    <?php
                    if(isset($_SESSION['updated'])){
                        echo $_SESSION['updated'];
                    }
                    $_SESSION['updated'] = null;
                    ?>

                    <?php
                    if(isset($_SESSION['deleted'])){
                        echo $_SESSION['deleted'];
                    }
                    $_SESSION['deleted'] = null;
                    ?>

                    <form action="" method="post">

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Order Date</label>
                            <div class="col-sm-6">
                                <input type="text" name="order_date" readonly class="form-control form-control-sm" value="<?php $time_zone = date_default_timezone_set("Asia/Dhaka");?><?= date("Y-m-d");?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Customer Name*</label>
                            <div class="col-sm-6">
                                <input type="text" name="customer_name" class="form-control form-control-sm" placeholder="Enter Customer Name">
                            </div>
                        </div>

                        <div class="jumbotron">
                            <h3>Make an Order List</h3>
                            <table align="center" style="width:100%;">
                                <thead>
                                <tr class="text-center">
                                    <th>Sl No.</th>
                                    <th>Product</th>
                                    <th>Price(MRP)</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php

                                $sub_total = 0;
                                if($orders){
                                    $i = 0;
                                    foreach($orders as $order):
                                        $i = $i + 1;
                                        $sub_total = $sub_total + $order['total_price'];

                                        ?>
                                        <tr class="text-center">
                                            <td><?= $i; ?></td>

                                            <td><input type="text" name="product_name" class="form-control form-control-sm" value="<?= $order['product_name'];?>" readonly></td>

                                            <td><input type="text" name="unit_price" class="form-control form-control-sm" value="<?= $order['unit_price'];?>" readonly></td>

                                            <td>

                                                <div class="input-group">
                                                    <input type="hidden" name="product_id" class="form-control form-control-sm" value="<?= $order['product_id'];?>">
                                                    <input type="hidden" name="unit_price" class="form-control form-control-sm" value="<?= $order['unit_price'];?>">
                                                    <input type="number" name="quantity" class="form-control form-control-sm" value="<?= $order['quantity'];?>" min="1" max="100">
                                                </div>

                                            </td>

                                            <td>$<?= $order['total_price'];?></td>
                                        </tr>

                                    <?php endforeach; }?>
                                
                                </tbody>
                            </table>
                        </div>

                        <div style="margin-top: 50px">

                            <div class="form-group row">
                                <label for="sub_total" class="col-sm-3 col-form-label">Sub Total (USD)</label>
                                <div class="col-sm-6">
                                    <input type="text" readonly name="sub_total" class="form-control form-control-sm" value="<?= $sub_total;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gst" class="col-sm-3 col-form-label">VAT (10%)</label>
                                <div class="col-sm-6">
                                    <input type="text" readonly name="vat" class="form-control form-control-sm" value="<?= $vat = ($sub_total + ($sub_total * (10 / 100)));?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="discount" class="col-sm-3 col-form-label">Discount (USD)*</label>
                                <div class="col-sm-6">
                                    <input type="text" name="discount" class="form-control form-control-sm" value="0" min="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="net_total" class="col-sm-3 col-form-label">Net Total (USD)</label>
                                <div class="col-sm-6">
                                    <input type="text" readonly name="net_total" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="paid" class="col-sm-3 col-form-label">Paid (USD)*</label>
                                <div class="col-sm-6">
                                    <input type="text" name="paid" class="form-control form-control-sm" value="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="due" class="col-sm-3 col-form-label">Due (USD)</label>
                                <div class="col-sm-6">
                                    <input type="text" readonly name="due" class="form-control form-control-sm" value="0.0" min="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="payment_type" class="col-sm-3 col-form-label">Payment Method</label>
                                <div class="col-sm-6">
                                    <select name="payment_type" class="form-control form-control-sm">
                                        <option>Cash</option>
                                        <option>Card</option>
                                        <option>Draft</option>
                                        <option>Cheque</option>
                                    </select>
                                </div>
                            </div>

                            <div align="center">
                                <button type="submit" name="purchase" style="width:150px;" class="btn btn-warning">PURCHASE</button>
                                <input type="submit" name="print_invoice" style="width:150px;" class="btn btn-success d-none" value="Print Invoice">
                            </div>

                        </div>

                </div> <!--Crad Body Ends-->
            </div>

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>

</body>
</html>
