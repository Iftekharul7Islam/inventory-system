<?php
session_start();

include_once("../src/db.php");


if($_SERVER['REQUEST_METHOD'] = 'POST' && isset($_POST['purchase'])){

    $s_id = $_SESSION['guest_user'];
    $customer_name = $_POST['customer_name'];
    $sub_total = $_POST['sub_total'];
    $vat = $_POST['vat'];
    $discount = $_POST['discount'];
    $net_total = $vat - $discount;
    $paid = $_POST['paid'];
    $due = $net_total - $paid;
    $payment_type = $_POST['payment_type'];
    $order_date = $_POST['order_date'];

    /*echo '<pre>';
    print_r($_POST);
    echo '</pre>';
    die();*/

    if(empty($customer_name) && empty($discount) && empty($paid)){
        $errors[] = '<div class="alert alert-danger"><strong>Error! </strong>Fields must not be empty!</div>';
    }elseif(empty($customer_name) || empty($discount) || empty($paid)){
        $errors[] = '<div class="alert alert-danger"><strong>Error! </strong>Field must not be empty!</div>';
    }else{

        $query = "INSERT INTO invoice_details 
(s_id, customer_name, sub_total, vat, discount, net_total, paid, due, payment_type, order_date) 
VALUES (:s_id, :customer_name, :sub_total, :vat, :discount, :net_total, :paid, :due, :payment_type, :order_date)";
        $sth = $conn->prepare($query);
        $sth->bindParam(':s_id', $s_id);
        $sth->bindParam(':customer_name', $customer_name);
        $sth->bindParam(':sub_total', $sub_total);
        $sth->bindParam(':vat', $vat);
        $sth->bindParam(':discount', $discount);
        $sth->bindParam(':net_total', $net_total);
        $sth->bindParam(':paid', $paid);
        $sth->bindParam(':due', $due);
        $sth->bindParam(':payment_type', $payment_type);
        $sth->bindParam(':order_date', $order_date);
        $result = $sth->execute();

        if($result){
            $msgs[] = '<div class="alert alert-success"><strong>Success! </strong>Invoice inserted successfully.</div>';
        }else{
            $msgs[] = '<div class="alert alert-danger"><strong>Error! </strong>Invoice insertion failed!</div>';
        }
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Product List</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>


<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-10 mx-auto">

                        <div class="card" style="box-shadow:0 0 15px 0 lightgrey;">
                            <div class="card-header">
                                <h4>Make an Order List
                                    <a href="manage_products.php" class="btn btn-dark btn-sm" style="float: right"><i class="far fa-plus-square"></i> Add</a>
                                </h4>
                            </div>
                            <div class="card-body">

                                <?php
                                if(!empty($errors)){
                                foreach($errors as $error){
                                echo $error;
                                }
                                }

                                if(!empty($msgs)){
                                foreach($msgs as $msg){
                                echo $msg;
                                }
                                }

                                ?>

                    <?php
                                if(isset($_SESSION['updated'])){
                                echo $_SESSION['updated'];
                                }
                                $_SESSION['updated'] = null;
                                ?>

                    <?php
                                if(isset($_SESSION['deleted'])){
                                echo $_SESSION['deleted'];
                                }
                                $_SESSION['deleted'] = null;
                                ?>

                                


                                    <!--<h3>Make an Order List</h3>-->
                                    <table align="center" style="width:100%;">
                                        <thead>
                                        <tr class="text-center">
                                            <th>Sl No.</th>
                                            <th>Product</th>
                                            <th>Price(MRP)</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <?php

                                        $s_id = $_SESSION['guest_user'];


                                        if(isset($s_id)){

                                            $query = 'SELECT * FROM invoices WHERE s_id = :s_id';
                                            $sth = $conn->prepare($query);
                                            $sth->bindParam(':s_id', $s_id);
                                            $sth->execute();

                                            $orders = $sth->fetchAll(PDO::FETCH_ASSOC);

                                            /*echo "<pre>";
                                            print_r($orders);
                                            echo "</pre>";
                                            die();*/
                                        }

                                        $sub_total = 0;
                                        if($orders){
                                            $i = 0;
                                            foreach($orders as $order):
                                                $i = $i + 1;
                                                $sub_total = $sub_total + $order['total_price'];

                                                ?>
                                                <tr class="text-center">
                                                    <td><?= $i; ?></td>

                                                    <td><input type="text" name="product_name" class="form-control form-control-sm" value="<?= $order['product_name'];?>" readonly></td>

                                                    <td><input type="text" name="unit_price" class="form-control form-control-sm" value="<?= $order['unit_price'];?>" readonly></td>

                                                    <td>
                                                        <form action="manage_orderlist.php" method="post">

                                                            <div class="input-group">
                                                                <input type="hidden" name="product_id" class="form-control form-control-sm" value="<?= $order['product_id'];?>">
                                                                <input type="hidden" name="unit_price" class="form-control form-control-sm" value="<?= $order['unit_price'];?>">
                                                                <input type="number" name="quantity" class="form-control form-control-sm" value="<?= $order['quantity'];?>" min="1" max="100">
                                                                <button type="submit" name="update" class="btn btn-primary btn-sm">Update</button>
                                                                <a href="manage_orderlist.php?product_id=<?= $order['product_id']; ?>" class="btn btn-danger btn-sm">Remove</a>

                                                            </div>
                                </form>
                                                    </td>

                                                    <td>$<?= $order['total_price'];?></td>
                                                </tr>

                                            <?php endforeach; }else{ ?>


                                            <tr>
                                                <td colspan="5" class="jumbotron">There is no orders available now! <a href="manage_products.php" class="btn btn-success btn-sm">Click Here</a> to add one on the list. </td>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>


                                <form action="" method="post">
                                    <div style="margin-top: 50px">

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Order Date</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="order_date" readonly class="form-control form-control-sm" value="<?php $time_zone = date_default_timezone_set("Asia/Dhaka");?><?= date("Y-m-d");?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Customer Name*</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="customer_name" class="form-control form-control-sm" placeholder="Enter Customer Name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="sub_total" class="col-sm-3 col-form-label">Sub Total (USD)</label>
                                            <div class="col-sm-6">
                                                <input type="text" readonly name="sub_total" class="form-control form-control-sm" value="<?= $sub_total;?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="gst" class="col-sm-3 col-form-label">VAT (10%)</label>
                                            <div class="col-sm-6">
                                                <input type="text" readonly name="vat" class="form-control form-control-sm" value="<?= $vat = ($sub_total + ($sub_total * (10 / 100)));?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="discount" class="col-sm-3 col-form-label">Discount (USD)*</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="discount" class="form-control form-control-sm" value="0" min="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="net_total" class="col-sm-3 col-form-label">Net Total (USD)</label>
                                            <div class="col-sm-6">
                                                <input type="text" readonly name="net_total" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="paid" class="col-sm-3 col-form-label">Paid (USD)*</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="paid" class="form-control form-control-sm" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="due" class="col-sm-3 col-form-label">Due (USD)</label>
                                            <div class="col-sm-6">
                                                <input type="text" readonly name="due" class="form-control form-control-sm" value="0.0" min="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="payment_type" class="col-sm-3 col-form-label">Payment Method</label>
                                            <div class="col-sm-6">
                                                <select name="payment_type" class="form-control form-control-sm">
                                                    <option>Cash</option>
                                                    <option>Card</option>
                                                    <option>Draft</option>
                                                    <option>Cheque</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div align="center">
                                            <button type="submit" name="purchase" style="width:150px;" class="btn btn-warning">PURCHASE</button>
                                      </div>

                                    </div>
                            </div> <!--Crad Body Ends-->

                                </form>
                            <!--<a href="print_invoice.php?s_id=<?/*= $s_id; */?>" name="print_invoice" class="btn btn-light btn-sm">Print Invoice</a>-->

                            <div class="card-footer"><h5 align="center">Inventory Management System</h5></div>
                        </div>

        </div>
    </div>
</div>





<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>

</body>
</html>