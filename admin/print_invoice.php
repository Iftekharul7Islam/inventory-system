<?php
session_start();

include_once("../src/db.php");

$s_id = $_GET['s_id'];

/*if(isset($s_id)){

    $query = 'SELECT * FROM invoices WHERE s_id = :s_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':s_id', $s_id);
    $sth->execute();

    $orders = $sth->fetchAll(PDO::FETCH_ASSOC);

    echo '<pre>';
    print_r($orders);
    echo '</pre>';
    die();

}*/

if (isset($s_id)){
    $query = 'SELECT * FROM invoice_details WHERE s_id = :s_id AND invoice_id = :invoice_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':s_id', $s_id);
    $sth->bindParam(':invoice_id', $invoice_id);
    $sth->execute();

    $invoices = $sth->fetch(PDO::FETCH_ASSOC);

    echo '<pre>';
    print_r($invoices);
    echo '</pre>';
    die();
}