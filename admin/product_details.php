<?php
session_start();

include_once("../src/db.php");

$product_id = $_GET['product_id'];

if(isset($product_id)) {
    
    $query = "SELECT products.*, categories.category_name, brands.brand_name FROM products
              INNER JOIN categories ON products.category_id = categories.category_id
              INNER JOIN brands ON products.brand_id = brands.brand_id
              WHERE products.product_id = :product_id";

    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);

    /*$query = 'SELECT * FROM products WHERE product_id = :product_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);

    $category_id = $product['category_id'];

    $query = 'SELECT * FROM categories WHERE category_id = :category_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':category_id', $category_id);
    $sth->execute();

    $category = $sth->fetch(PDO::FETCH_ASSOC);

    $brand_id = $product['brand_id'];

    $query = 'SELECT * FROM brands WHERE brand_id = :brand_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':brand_id', $brand_id);
    $sth->execute();

    $brand = $sth->fetch(PDO::FETCH_ASSOC);*/
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Products</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>
<div class="container" style="margin-top: 50px">
    <div class="card text-center mx-auto" style="width: 50%">
        <div class="card-header">
            <h3>Product Details</h3>
        </div>
        <div class="card-body">
            <form action="addtoorderlist.php" method="post">
                <input type="hidden" name="product_id" value="<?=$product['product_id'];?>">
                <h4 class="card-title"><?=$product['product_name'];?></h4>
                <p class="card-text"><?=$product['category_name'];?> : <?=$product['brand_name'];?></p>
                <p class="card-text">$<?=$product['mrp'];?></p>
                <input type="number" name="quantity" value="1" min="1">
                <button type="submit" class="btn btn-outline-primary" name="add-to-order">Add to Order</button>
            </form>

        </div>
        <div class="card-footer text-muted">
            Inventory Management System
        </div>
    </div>
</div>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>