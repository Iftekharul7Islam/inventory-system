<?php
session_start();
include_once("../src/db.php");

$user_id = $_SESSION['id'];

if(isset($user_id)){

    $query = 'SELECT * FROM users WHERE id = :id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':id', $user_id);
    $sth->execute();

    $user_info = $sth->fetch(PDO::FETCH_ASSOC);
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update-profile'])) {

    $user_name = $_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $prev_picture_name = $_POST['prev_picture_name'];

    /*echo '<pre>';
    print_r($_FILES);
    echo '</pre>';
    die();*/

    $time_zone = date_default_timezone_set("Asia/Dhaka");

    $uploaded = false;

    if($_FILES['picture']['size'] > 0){
        $target_file = $_FILES['picture']['tmp_name'];
        //$filename = time().'_'.str_replace(' ', '-',$_FILES['picture']['name']);
        $filename = $_FILES['picture']['name'];
        $destination_file = $_SERVER['DOCUMENT_ROOT'].'/inventory-system/uploads/'.$filename;

        $uploaded = move_uploaded_file($target_file, $destination_file);
    }

    if($uploaded){
        $destination_filename = $filename;
    }else{
        $destination_filename = $prev_picture_name;
    }

    if(empty($user_name) && empty($name) && empty($email)){
        $errors[] = "<div class='alert alert-danger'>Fields must not be empty!</div>";
    }elseif(empty($user_name) || empty($name) || empty($email)){
        $errors[] = "<div class='alert alert-danger'>Field must not be empty!</div>";
    }else {
        $query = "UPDATE users
                  SET 
                      name = :name,
                      username = :username, 
                      email = :email,
                      picture = :picture
                  WHERE id = :id";

        $sth = $conn->prepare($query);
        $sth->bindParam(':name', $name);
        $sth->bindParam(':username', $user_name);
        $sth->bindParam(':email', $email);
        $sth->bindParam(':picture', $destination_filename);
        $sth->bindParam(':id', $user_id);
        $result = $sth->execute();

        if($result){
            $_SESSION['updated'] = "<div class='alert alert-success'>Profile updated successfully.</div>";
            header("location:index.php");
        }else{
            $_SESSION['updated'] = "<div class='alert alert-danger'>Profile not updated!</div>";
        }
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Inventory Management System</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 30rem;">
        <div class="card-header"><h5>Edit Profile</h5></div>
        <div class="card-body">

            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            ?>

            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input
                        type="text"
                        onclick="this.select()"
                        value="<?= $user_info['username'];?>"
                        name="username"
                        class="form-control"
                        id="username"
                        autofocus="autofocus">
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        onclick="this.select()"
                        value="<?= $user_info['name'];?>"
                        name="name"
                        class="form-control"
                        id="name">
                </div>

                <div class="form-group">
                    <input
                        type="email"
                        onclick="this.select()"
                        value="<?= $user_info['email'];?>"
                        name="email"
                        class="form-control"
                        id="email">
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="prev_picture_name"
                        value="<?=$user_info['picture']?>"
                        class="form-control"
                        id="prev_picture_name">
                </div>

                <div class="form-group">
                    <input
                        type="file"
                        onclick="this.select()"
                        value=""
                        name="picture"
                        class="form-control"
                        id="picture">
                    <?php if(!empty($user_info['picture'])){ ?>
                        <img class="card-img-top mx-auto"
                             style="width: 50%;"
                             src="../uploads/<?=$user_info['picture'];?>" alt="Profile Picture">
                    <?php }else{ ?>
                        <div>No image is available. Please upload one.</div>
                    <?php }?>
                </div>

                <button type="submit" class="btn btn-primary" name="update-profile">Update</button>
            </form>
            
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>
