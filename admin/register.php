<?php
session_start();

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

include_once("../src/db.php");

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register-btn'])){

    $username = $_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm-password'];

    /*echo '<pre>';
    print_r($_FILES);
    echo '</pre>';
    die();*/

    $time_zone = date_default_timezone_set("Asia/Dhaka");
    
    $target_file = $_FILES['picture']['tmp_name'];
    //$filename = time().'_'.str_replace(' ', '-',$_FILES['picture']['name']);
    $filename = $_FILES['picture']['name'];
    $destination_file = $_SERVER['DOCUMENT_ROOT'].'/inventory-system/uploads/'.$filename;

    $uploaded = move_uploaded_file($target_file, $destination_file);
    //die($uploaded);

    if($uploaded){
        $destination_filename = $filename;
    }else{
        $destination_filename = "";
    }


    if(empty($username) && empty($name) && empty($email) && empty($password) && empty($confirmPassword)){
        $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Fields must not be empty!</div>";
    }elseif(empty($username) || empty($name) || empty($email) || empty($password) || empty($confirmPassword)){
        $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
    }else {

        if (strlen($username) < 6) {
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Username is too short!</div>";
        }

        if (preg_match("/[^a-zA-Z0-9_-]+/i", $username)) {
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Username must contain alphanumerics, dashes and underscores!</div>";
        }else {
            $query = 'SELECT username FROM users WHERE username = :username';
            $sth = $conn->prepare($query);
            $sth->bindParam(':username', $username);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);

            if (!empty($row['username'])) {
                $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Username is already exist!</div>";

            }
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
        }else{
            $query = 'SELECT email FROM users WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);

            if (!empty($row['email'])) {
                $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Email address is already exist!</div>";
            }
        }


        if(strlen($password) <6){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Password is too short!</div>";
        }

        if(strlen($password) > 32){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Password is too long!</div>";
        }

        if($password != $confirmPassword){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Passwords do not match!</div>";
        }

        if(empty($errors)) {

            $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
            //$email = $_SESSION['email'];

            $token = bin2hex(random_bytes(50));
            $verified = false;

            $query = "INSERT INTO users(name, username, email, token, verified, password, picture) 
                      VALUES(:name, :username, :email, :token, :verified, :password, :picture)";
            $sth = $conn->prepare($query);
            $sth->bindParam(':name', $name);
            $sth->bindParam(':username', $username);
            $sth->bindParam(':email', $email);
            $sth->bindParam(':token', $token);
            $sth->bindParam(':verified', $verified);
            $sth->bindParam(':password', $hashedPassword);
            $sth->bindParam(':picture', $destination_filename);
            $result = $sth->execute();

           if($result){
               /*$_SESSION['verified'] = false;*/

               //**********************

               // Instantiation and passing `true` enables exceptions
               $mail = new PHPMailer(true);

               try {
                   //Server settings
                   $mail->isSMTP();                                            // Send using SMTP
                   $mail->Host       = 'smtp.mailtrap.io';                    // Set the SMTP server to send through
                   $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                   $mail->Username   = 'ccf4b98500b6aa';                     // SMTP username
                   $mail->Password   = '4c132260973429';                               // SMTP password
                   $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                   $mail->Port       = 587;                                    // TCP port to connect to

                   //Recipients
                   $mail->setFrom('admin@eshikhon.com', 'Inventory');
                   $mail->addAddress($email, $name);     // Add a recipient
                   $mail->addReplyTo('no-reply@gmail.com', 'No Reply');


                   // Content
                   $mail->isHTML(true);                                  // Set email format to HTML
                   $mail->Subject = 'Reset your password';
                   $mail->Body    = '<p>Hello '.$name.',</br></br>

                                        Thank you for signing up on our website. Please click on the link below to verify your email.
                                      </p><a class="btn btn-primary" href="http://localhost/inventory-system/admin/user_verification.php?verification-token='.$token.'">Verify your Email</a>';
                   $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                   $mail->send();
               } catch (Exception $e) {
                   echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
               }

               //**********************

               $msgs[] = "<div class='alert alert-success'><strong>Thank You! </strong>Your registration is successful.</div>";
               $msgs[] = "<div class='alert alert-warning'>You need to verify your account. Sign in to your email account
                          and click on the verification link we just emailed you at <strong>$email</strong></div>";

            }else{
               $msgs[] = "<div class='alert alert-danger'><strong>Sorry! </strong>There are some problem with your details!</div>";
            }

        }
        }
}

?>



<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Registration</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 30rem;">
        <div class="card-header">Registration</div>
        <div class="card-body">
            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            if(!empty($msgs)){
                foreach($msgs as $msg){
                    echo $msg;
                }
            }


            ?>
            <form action="" method="POST" enctype="multipart/form-data">

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['username'];}?>"
                           name="username"
                           class="form-control"
                           id="username"
                           placeholder="Enter username"
                           autofocus="autofocus" >
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['name'];}?>"
                           name="name"
                           class="form-control"
                           id="name"
                           placeholder="Enter name"
                           autofocus="autofocus" >
                </div>

                <div class="form-group">
                    <input type="email"
                           value="<?php if(!empty($errors)){echo $_POST['email'];}?>"
                           name="email"
                           class="form-control"
                           id="email"
                           placeholder="Enter email"
                           autofocus="autofocus" >
                </div>

                <div class="form-group">
                    <input type="password"
                           name="password"
                           class="form-control"
                           id="password"
                           placeholder="Enter password"
                           autofocus="autofocus" >
                </div>

                <div class="form-group">
                    <input type="password"
                           name="confirm-password"
                           class="form-control"
                           id="confirm-password"
                           placeholder="Confirm password"
                           autofocus="autofocus" >
                </div>

                <div class="form-group">
                    <input type="file"
                           name="picture"
                           id="picture"
                           class="form-control">
                </div>

                <div class="form-group">
                    <select id="usertype"
                            name="usertype"
                            class="form-control"
                            autofocus="autofocus" >
                        <option selected>Admin</option>
                        <option>Other</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-dark" name="register-btn"><i class="fa fa-user"></i> Register</button>
                <span><a href="login.php">Login</a></span>

            </form>
        </div>
        <div class="card-footer"><a href="#"></a></div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>