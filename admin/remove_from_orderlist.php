<?php

session_start();

include_once("../src/db.php");

$product_id = $_GET['product_id'];
$s_id = $_SESSION['guest_user'];

if(isset($product_id)){
    $query = "DELETE FROM invoices WHERE product_id = :product_id AND s_id = :s_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->bindParam(':s_id', $s_id);
    $result = $sth->execute();

    if($result){

        //$_SESSION['deleted'] = "<div class='alert alert-success'>Item removed from list successfully.</div>";
        header("location:orderlist.php");
    }else{
        //$_SESSION['deleted'] = "<div class='alert alert-danger'>Item does not removed!</div>";
    }
}