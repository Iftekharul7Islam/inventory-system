<?php
session_start();
include_once("../src/db.php");

if(isset($_SESSION['id'])){
    header('Location: index.php');
}

$password_token = $_GET['password-token'];

if(isset($password_token)){

    $query = 'SELECT * FROM users WHERE token = :token';
    $sth = $conn->prepare($query);
    $sth->bindParam(':token', $password_token);
    $sth->execute();

    $user_info = $sth->fetch(PDO::FETCH_ASSOC);

    /*echo '<pre>';
    print_r($user_info);
    echo '</pre>';
    die();*/

    //$_SESSION['id'] = $user_info['id'];
    $_SESSION['email'] = $user_info['email'];

    //echo $_SESSION['id'];
   // echo $_SESSION['email'];
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['reset-password'])) {

    /*echo '<pre>';
    print_r($_POST);
    echo '</pre>';
    die();*/

    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm-password'];
    $email = $_SESSION['email'];

    if(empty($password) && empty($confirmPassword)){
        $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Fields must not be empty!</div>";
    }elseif(empty($password) || empty($confirmPassword)){
        $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
    }else{

        if(strlen($password) <6){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Password is too short!</div>";
        }

        if(strlen($password) > 32){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Password is too long!</div>";
        }

        if($password != $confirmPassword){
            $errors[] = "<div class='alert alert-danger'><strong>Error! </strong>Passwords do not match!</div>";
        }


        if(empty($errors)) {

            $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

            $query = "UPDATE users
                      SET password = :password
                      WHERE email = :email";
            
            $sth = $conn->prepare($query);
            $sth->bindParam(':password', $hashedPassword);
            $sth->bindParam(':email', $email);
            $result = $sth->execute();

            if($result){
                $_SESSION['updated'] = "<div class='alert alert-success'>Password updated successfully. Please Login </div>";
                header('location: login.php');
            }else{
                $_SESSION['updated'] = "<div class='alert alert-danger'><strong>Password not updated!</div>";
            }

            }

        }
    
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Reset Password</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 18rem;">
        <img class="card-img-top mx-auto" style="width: 60%" src="../lib/img/reset_password.png" alt="Reset Password Icon">
        <div class="card-body">

            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            if(!empty($msgs)){
                foreach($msgs as $msg){
                    echo $msg;
                }
            }

            ?>

            <form action="" method="post">

                <div class="form-group">
                    <input type="password"
                           name="password"
                           class="form-control"
                           id="password"
                           placeholder="Enter New Password">
                </div>

                <div class="form-group">
                    <input type="password"
                           name="confirm-password"
                           class="form-control"
                           id="confirm-password"
                           placeholder="Re - enter New Password">
                </div>

                <button type="submit" class="btn btn-dark" name="reset-password"><i class="fas fa-sign-in-alt"></i> Reset Password</button>

            </form>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>