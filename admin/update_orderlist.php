<?php

session_start();

include_once("../src/db.php");

/*echo "<pre>";
print_r($_POST);
echo "</pre>";
die();*/

$product_id = $_POST['product_id'];
$unit_price = $_POST['unit_price'];
$quantity = $_POST['quantity'];
$s_id = $_SESSION['guest_user'];

$total_price = $quantity * $unit_price;

$query = "UPDATE invoices
          SET 
          quantity = :quantity, 
          total_price = :total_price
          
          WHERE product_id = :product_id AND s_id = :s_id";

$sth = $conn->prepare($query);
$sth->bindParam(':quantity', $quantity);
$sth->bindParam(':total_price', $total_price);
$sth->bindParam(':product_id', $product_id);
$sth->bindParam(':s_id', $s_id);
$result = $sth->execute();

if($result){

    header('Location:orderlist.php');
}

