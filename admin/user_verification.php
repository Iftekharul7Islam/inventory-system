<?php
session_start();

include_once("../src/db.php");

$verification_token = $_GET['verification-token'];

if(isset($verification_token)){

    $query = 'SELECT * FROM users WHERE token = :token';
    $sth = $conn->prepare($query);
    $sth->bindParam(':token', $verification_token);
    $sth->execute();

    $user_info = $sth->fetch(PDO::FETCH_ASSOC);

    $verified = $user_info['verified'];
    $verified = true;

    $query = "UPDATE users
              SET verified = :verified
              WHERE token = :token";

    $sth = $conn->prepare($query);
    $sth->bindParam(':verified', $verified);
    $sth->bindParam(':token', $verification_token);
    $result = $sth->execute();
    
    if($result){
        $_SESSION['id'] = $user_info['id'];
        $_SESSION['verified'] = "<div class='alert alert-success'>Your email address is successfully verified. You are now logged in.</div>";
        header('Location:index.php');
    }else{
        $_SESSION['verified'] = "<div class='alert alert-danger'>Your email address was not verified! Please check again.</div>";
    }
}