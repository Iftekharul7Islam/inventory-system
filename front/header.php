<?php

if(array_key_exists('guest_user',$_SESSION) && !empty($_SESSION['guest_user'])){
    //echo $_SESSION['guest_user'];
}else{
    
    $_SESSION['guest_user'] = 'guest_user_'.time();
}

if(isset($_GET['logout'])){
    session_unset();
    session_destroy();
    
    header('Location:login.php');
}


?>
<div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Inventory System</a>
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <?php
                if(isset($_SESSION['id'])){

                ?>

                <li class="nav-item active">
                    <a class="nav-link" href="index.php"><i class="fa fa-home"></i> Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="?logout=true"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </li>

                <?php }?>
            </ul>
        </div>
    </nav>
</div>
