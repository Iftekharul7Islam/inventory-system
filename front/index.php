<?php
session_start();

include_once("../src/db.php");

/*if($_SESSION['id'] == null){
    header('Location: login.php');
}*/

if(isset($_SESSION['id'])) {

    $user_id = $_SESSION['id'];

    $query = 'SELECT * FROM users WHERE id = :id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':id', $user_id);
    $sth->execute();

    $user_info = $sth->fetch(PDO::FETCH_ASSOC);
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">

    <link rel="stylesheet" href="../lib/font/css/all.min.css">

    <title>Inventory Management System</title>
</head>
<body>

<!--Navbar-->
<?php include_once("header.php"); ?>

<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top mx-auto"
                     style="width: 100%;"
                     src="../uploads/<?php if(isset($_SESSION['id'])){echo $user_info['picture'];}?>" alt="Profile Picture">

                <div class="card-body">
                    <?php
                    if(isset($_SESSION['id'])){
                        echo $_SESSION['login-msg'];
                    }

                    $_SESSION['login-msg'] = NULL;

                    ?>

                    <?php
                    if(isset($_SESSION['id'])){
                        echo $_SESSION['updated'];
                    }

                    $_SESSION['updated'] = NULL;

                    ?>
                    <h5 class="card-title">Profile Info</h5>
                    <p class="card-text"><i class="fa fa-user"></i>
                    <?php echo $user_info['name']; ?>
                    </p>
                    <p class="card-text"><i class="fa fa-user"></i> Admin</p>
                    <p class="card-text"><i class="far fa-clock"></i> <strong>Last Login:</strong> <?php $time_zone = date_default_timezone_set("Asia/Dhaka");?><?= date("Y-m-d h:i:s");?></p>
                    <a href="profile.php" class="btn btn-dark"><i class="far fa-edit"></i> Edit Profile</a>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="jumbotron" style="width: 100%; height: 100%;">
                <h1>Welcome <?php echo $user_info['username']; ?></h1>
                <div class="row">
                    <div class="col-sm-6">
                        <iframe src="http://free.timeanddate.com/clock/i71uc0yq/n73/szw120/szh120/hoc000/hbw4/cf100/hgr0/hcw2/fav0/fiv0/mqc000/mqs3/mql25/mqw6/mqd96/mhc000/mhs3/mhl20/mhw6/mhd96/mmc000/mms3/mml10/mmw2/mmd96/hhw16/hmw16/hmr4/hsc000/hss3/hsl90"
                                frameborder="0" width="160" height="160"></iframe>
                    </div>

                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">New Orders</h5>
                                <p class="card-text">Here you can make invoices and create new orders</p>
                                <a href="orderlist.php" class="btn btn-dark"><i class="far fa-calendar-plus"></i> New Orders</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Categories</h5>
                    <p class="card-text">Here you can add and manage categories</p>
                    <a href="add_categories.php" class="btn btn-dark">Add</a>
                    <a href="manage_categories.php" class="btn btn-dark">Manage</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Brands</h5>
                    <p class="card-text">Here you can add and manage brands</p>
                    <a href="add_brands.php" class="btn btn-dark" >Add</a>
                    <a href="manage_brands.php" class="btn btn-dark">Manage</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Products</h5>
                    <p class="card-text">Here you can add and manage products</p>
                    <a href="add_products.php" class="btn btn-dark">Add</a>
                    <a href="manage_products.php" class="btn btn-dark">Manage</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../lib/js/jquery-3.4.1.min.js"></script>
<script src="../lib/js/popper.min.js"></script>
<script src="../lib/js/bootstrap.min.js"></script>
<script src="../lib/font/js/all.min.js"></script>
<script src="../lib/js/main.js"></script>
</body>
</html>