<?php

if(version_compare(phpversion(), '5.4.0', '<')){
    if(empty(session_id())){
        session_start();
    }
}else{
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
}

//connect to database
$servername = "localhost";
$dbusername = "root";
$dbpassword = "";
$dbname = "inventorysys";

//connection string : connecting to database
try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $dbusername, $dbpassword);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'Connection failed '.$e->getMessage();
}